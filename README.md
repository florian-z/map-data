# Map Converting stuff

Currently this code is able to convert german map data from a csv into international
map data controlled by last imported data from
international stage.

### Usage

```bash
cp data/control_data.csv.empty data/control_data.csv
cp data/german.csv.empty data/german.csv
cp data/output.csv.empty data/output.csv 
```

Extract last tab from int. data as csv and copy it inot the `data/control_data.csv` file. Copy your german map data into 
`data/german.csv`

and then run:

```bash
php bin/convert.php 
```

This result can be found inside `data/output.csv` and should be in a format of the international data. Please copy content 
into the override tab there.
